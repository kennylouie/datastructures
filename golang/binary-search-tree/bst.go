package bst // (b)inary (s)earch (t)ree

import (
	"errors"
)

// Introduction

// The following go program will implement a binary search tree

// Motivation
// Linear data containers, i.e. array or list, can take up to O(n) time to search for an element.
// A tree can conduct this operation faster, including more efficient insertion or deletion than their linear counter-parts.

// A tree is a hierarchical structure, i.e. organizes information by hierarchy and relationships
// The relationship is as follows:
// A tree consists of nodes that hold unique values
// A binary tree has 0, 1, or 2 child nodes
// One of these nodes is the root node that is at the top of the tree structure and is the entry point of operation
// Each node has exactly 1 parent node except the root node which has none
// A binary search tree is where the value of left node has a lesser or equal value to the parent node
// both the parent and left node values are lesser than the right node
// The height of the tree is defined as the path from the root node to a leaf node

// The advantage of the binary search tree is that a search operation has an order of O(log(n)) in its best case, which is much faster than O(n) for linear containers.
// This is due to the fact that traversing through a tree of n elements has a height of log(base2)(n+1) for a balanced tree, i.e. the height to any leaf is about the same.

type Node struct {
	Value string // key
	Data  string
	Left  *Node
	Right *Node
}

// Each chunk of code below will implement methods that will overall enable a binary search tree

// Search
func (n *Node) Search(value string) (string, bool) {

    if n == nil {
        return "", false
    }

    switch {

    case value == n.Value:
        return n.Data, true

    case value < n.Value:
        return n.Left.Search(value)

    default:
        return n.Right.Search(value)

    }

}

// Insert
func (n *Node) Insert(value, data string) error {

    if n == nil {
        return errors.New("Cannot insert a value into a nil tree")
    }

    switch {

    case value == n.Value:
        return nil

    case value < n.Value:
        if n.Left == nil {
            n.Left = &Node{
                Value: value,
                Data: data
            }
            return nil
        }
        return n.Left.Insert(value, data)

    case value > n.Value:
        if n.Right == nil {
            n.Right = &Node{
                Value: value,
                Data: data
            }
            return nil
        }
        return n.Right.Insert(value, data)
    }

    return nil
}

// Delete
func (n *Node) Delete(value string, parent *Node) error {

    if n == nil {
        return errors.New("Value to be deleted does not exist in the tree")
    }

    switch {

    // traverse until node is the value to delete
    case value < n.Value:
        return n.Left.Delete(value, n)

    case value > n.Value:
        return n.Right.Delete(value, n)

    default:
        // leaf
        if n.Left == nil && n.Right == nil {
            n.replaceNode(parent, nil)
            return nil
        }

        // half-leaf
        if n.Left == nil {
            n.replaceNode(parent, n.Right)
        }
        // half-leaf
        if n.Right == nil {
            n.replaceNode(parent, n.Left)
            return nil
        }

        // inner node
        replacement, replParent := n.Left.findMax(n)

        n.Value = replacement.Value
        n.Data = replacement.Data

        return replacement.Delete(replacement.Value, replParent)
    }

}

// Max
func (n *Node) findMax(parent *Node) (*Node, *Node) {

    if n == nil {
        return nil, parent
    }

    if n.Right == nil {
        return n, parent
    }

    return n.Right.findMax(n)

}

// helper functions
func (n *Node) replaceNode(parent, replacement *Node) error {

    if n == nil {
        return errors.New("replaceNode() not allowed on a nil node")
    }

    if n == parent.Left {
        parent.Left = replacement
        return nil
    }

    parent.Right = replacement
    return nil

}
